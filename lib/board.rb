require 'byebug'

class Board

  def initialize(grid=[[nil,nil,nil],[nil,nil,nil],[nil,nil,nil]])
    @grid = grid
  end

  attr_accessor :grid

  def place_mark(pos,mark)
    @grid[pos[0]][pos[1]] = mark unless @grid[pos[0]][pos[1]]
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]] == nil
  end

  def winner # => ABSOLUTLEY REFACTOR THIS. - try a search crawler.

    if any_winners?(@grid)
      return find_winner(@grid)

    elsif any_winners?(my_transpose(@grid))
      return find_winner(my_transpose(@grid))

    elsif diagonal_winners?
      return @grid[1][1]

    else
      return nil
    end
  end

  def over?
    winner || cats?
  end

  def my_transpose(arr)

    transposed_arr = []
    arr.length.times {transposed_arr<< []}

    arr.each_with_index do |sub_arr,idx|
      sub_arr.each_with_index do |el,sec_idx|
        transposed_arr[sec_idx] << el #just push it in,
      end
    end

    transposed_arr
  end

  private

  def any_winners?(grid)
    grid.any?{|el| el.count(el.first) == 3 &&el.first != nil}
  end

  def find_winner(grid)
    grid.select do |el|
      el.count(el.first) == 3 && el.first != nil
    end [0][0]
  end

  def diagonal_winners?
    @grid1 = [@grid[0][0],@grid[1][1],@grid[2][2]]
    @grid2 = [@grid[0][2],@grid[1][1],@grid[2][0]]

    (@grid1.uniq.size == 1 && @grid[0][0] != nil) ||
        (@grid2.uniq.size == 1 && @grid[0][2] != nil)
  end

  def cats?
    @grid.flatten.compact.length == 9 #remember compact
  end

end

