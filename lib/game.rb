require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = player_one
  end

  attr_accessor :board
  attr_reader   :current_player

  def play_turn
    @move = @current_player.get_move
    @board.place_mark(@move, @current_player.mark)
    switch_players!
  end

  def switch_players!
    @current_player == @player_one ?
          @current_player = @player_two : @current_player = @player_one
  end
end

#Single player.
#Let's Play!
#Display and get move,
# Computer's move:
# Display and get move.
# Check for winner and return the last move and exit if there was a winner.
