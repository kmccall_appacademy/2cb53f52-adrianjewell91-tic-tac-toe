class ComputerPlayer

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  attr_reader :name
  attr_accessor :board
  attr_accessor :mark

  def get_move

    return "No board or mark assigned." if !@board

    if winning_move_available?(@board.grid)
      return return_position(@board.grid)

    elsif winning_move_available?(@board.my_transpose(@board.grid))
      return return_position(@board.my_transpose(@board.grid)).reverse

    elsif diag?([@board.grid[0][0],@board.grid[1][1],@board.grid[2][2]])
      return diag_position( [[0,0],[1,1],[2,2]] )

    elsif diag?([@board.grid[0][2],@board.grid[1][1],@board.grid[2][0]])
      return diag_position( [[0,2],[1,1],[2,0]] )

    else
      return find_random_position
    end

  end

  private

  def winning_move_available?(grid)
    grid.any?{|el| el.count(nil) == 1 && el.compact.uniq.size == 1}
  end

  def return_position(grid)
    @arr = grid.select do |el|
      el.count(nil) == 1 && el.compact.uniq.size == 1
      end
    @row = grid.index(@arr.flatten)
    @col = @arr.flatten.index(nil)

    [@row,@col]
  end

  def diag?(arr)
    arr.compact.uniq.size == 1 && arr.count(nil) == 1
  end

  def diag_position(arr)
    arr.select{|el| @board.grid[el[0]][el[1]]==nil}.flatten
  end

  def find_random_position
    @pos = [rand(3),rand(3)]
    until board.grid[@pos[0]][@pos[1]] == nil
      @pos = [rand(3),rand(3)]
    end
    @pos
  end
end

#Could I use a block to do this?

